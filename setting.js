import axios from "axios";

axios.defaults.baseURL = "http://0.0.0.0:8080";
axios.defaults.headers.common["Content-Type"] =
  "application/json;charset=UTF-8";
axios.defaults.headers.common["Access-Control-Allow-Origin"] =
  "http://0.0.0.0:8080";
