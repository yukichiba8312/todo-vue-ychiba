import { createApp } from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import { loadFonts } from "./plugins/webfontloader";
import VCalendar from "v-calendar";
import "v-calendar/dist/style.css";

loadFonts();

createApp(App).use(vuetify).use(VCalendar).mount("#app");
